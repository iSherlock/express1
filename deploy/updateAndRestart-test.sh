#!/bin/bash

docker stop express-test

# Delete the old repo
rm -rf /home/ec2-user/express1

# any future command that fails will exit the script
set -e

# BE SURE TO UPDATE THE FOLLOWING LINE WITH THE URL FOR YOUR REPO
git clone https://gitlab.com/Isherlock/express1.git
git checkout test
cd /home/ec2-user/express1

# run the node app in a container
deploy/express-test.sh -d

