#!/bin/bash
docker run --network isolated_nw --name expressTest --rm $1 \
        -v /home/ec2-user/express1:/mnt/stuff \
        -w /mnt/stuff node:alpine npm run firststart